# Ernestine's Recipes

## Intro

This is more of a proof of concept at the moment, but the site will be used to save recipes and come back to it when needed.

## Goals
- Familiarize myself with Gridsome
- Connect a headless CMS to Gridsome's GraphQL Layer
- Implement a simplified search that will search within the recipes submitted


## Tech Used

- Vue / Javascript
- GraphQL
- HTML
- CSS
- Figma for mockups

## Services Used

- [Cloudinary](https://cloudinary.com/) -- Image Hosting
- [GitLab](https://gitlab.com/) -- Git Repo
- [Netlify](https://www.netlify.com/) -- Static File Hosting and deployment
- [Contentful](https://www.contentful.com/) -- Headless CMS
