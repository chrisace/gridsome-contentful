import "normalize.css";
// import "~/assets/scss/main.scss";
import "~/assets/css/style.css";
import DefaultLayout from "~/layouts/Default.vue";
// import { render } from "node-sass";

export default function(Vue) {
  Vue.component("Layout", DefaultLayout);
}
