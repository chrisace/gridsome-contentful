module.exports = {
  siteName: "Ernestine's Recipes",
  siteDescription: "All the coolest recipes",
  templates: {
    ContentfulBlogPost: "/blog/:blogSlug",
    ContentfulRecipes: "/recipes/:recipeSlug",
    // Tag: "/tag/:tag",
    // Tags: '/tags/:id'
  },
  plugins: [
    {
      use: "@gridsome/source-contentful",
      options: {
        space: process.env.CONTENTFUL_SPACE, // required
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN, // required
        host: "cdn.contentful.com",
        environment: "master",
        typeName: "Contentful",
        // refs: {
        //   // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
        //   tags: {
        //     typeName: "Tag",
        //     create: true,
        //   },
        // },
      },
    },
  ],
  configureWebpack: {
    // merged with the internal config
    node: {
      fs: "empty",
    },
  },
};
