{
  allContentfulBlogPost {
    edges {
      node {
        blogTitle
        id
        blogSlug
        blogSummary
        blogImagePreview {
          file {
            url
          }
          title
        }
        blogDate
        blogPost
      }
    }
  }
}



query ContentfulBlogPost($path: String!) {
  post: contentfulBlogPost(path: $path) {
    blogTitle
    blogPost(html)
  }
}